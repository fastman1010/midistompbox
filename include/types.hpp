#include <Arduino.h>

#ifndef TYPES
#define TYPES
#define MENU_SIZE       3
#define TITLE_LENGTH    9
#define NUM_SLOTS       6
#define MIN_INPUT_CHAR  32 // whitespace
#define MAX_PATTERNNR   95
#define MAX_SONGNR      23
#define TEMPO_SLOW      30
#define TEMPO_FAST      240

struct songEntry {
    char title[TITLE_LENGTH];
    uint8_t patterns[NUM_SLOTS];
    uint8_t tempo;
};

enum viewsEnum: uint8_t {
    VIEW_MAIN,
    VIEW_EDIT_TEMPO,
    VIEW_EDIT_SLOT,
    VIEW_SONG_SELECT,
    VIEW_SONG_ACTION,
    VIEW_SONG_TITLE_INPUT    
};

enum songActionsEnum: uint8_t {
    ACTION_LOAD,
    ACTION_SAVE,
    ACTION_BACK
};

#endif