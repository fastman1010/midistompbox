#include <Arduino.h>
#include "types.hpp"

#ifndef DRAWING_FUNCTIONS
#define DRAWING_FUNCTIONS

#define COLOR_NORMAL    1
#define COLOR_INVERTED  0
#define FONT_BIG        u8g2_font_VCR_OSD_mr // monospace, restricted charset, 12x15
#define FONT_SMALL      u8g2_font_victoriamedium8_8r // 8x8 box, restricted charset

extern void drawMain(U8G2* u8g2, songEntry* song, bool midiPlaying);
extern void drawSavedSong(U8G2* u8g2, songEntry* song, int8_t index);
extern void drawMenuActions(U8G2* u8g2, PGM_P const* actionNames, int8_t selectedIndex);
extern void drawTitleInput(U8G2* u8g2, char* title, int8_t selectedIndex);
extern void redrawTempo(U8G2* u8g2, int8_t color, uint8_t tempo);
extern void redrawSlots(U8G2* u8g2, uint8_t* patterns, int8_t selectedSlot = -1, int8_t playingSlot = -1);
extern void redrawPlayStop(U8G2* u8g2, bool play);
char* getSlotLabel(uint8_t index, bool isPlaying);
#endif