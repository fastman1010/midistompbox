#include <LiquidCrystal_74HC595_SPI.h>
#include <SPI.h>

LiquidCrystal_74HC595_SPI::LiquidCrystal_74HC595_SPI(uint8_t rs, uint8_t e) {
    _rs = rs;
    _e = e;
    _spiSettings = SPISettings(SPI_CLOCK_DIV4, MSBFIRST, SPI_MODE0);
}

void LiquidCrystal_74HC595_SPI::begin(uint8_t cols, uint8_t rows, uint8_t charsize) {
    _cols = cols;
    _rows = rows;
    _displayControl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;
    _displayMode = LCD_ENTRYLEFT | LCD_ENTRYSHIFTDECREMENT;

    pinMode(_rs, OUTPUT);
    pinMode(_e, OUTPUT);
    digitalWrite(_rs, LOW);
    digitalWrite(_e, LOW);

    digitalWrite(SS, LOW);
    SPI.begin();

    /* Special case of Function Set sent thrice */
    delayMicroseconds(120000U);
    command(LCD_FUNCTIONSET | LCD_8BITMODE);
    delayMicroseconds(5000);
    command(LCD_FUNCTIONSET | LCD_8BITMODE);
    delayMicroseconds(120);
    command(LCD_FUNCTIONSET | LCD_8BITMODE);
    delayMicroseconds(120);

    /* Now let's do real stuff */
    if (_rows > 1) {
        command(LCD_FUNCTIONSET | LCD_8BITMODE | LCD_2LINE | LCD_5x8DOTS);
    } else if (_rows == 1 && charsize != 0) {
        command(LCD_FUNCTIONSET | LCD_8BITMODE | LCD_1LINE | LCD_5x10DOTS);
    } else {
        command(LCD_FUNCTIONSET | LCD_8BITMODE | LCD_1LINE | LCD_5x8DOTS);
    }
    delayMicroseconds(60);

    command(LCD_DISPLAYCONTROL);
    delayMicroseconds(60);

    clear();
    
    command(LCD_ENTRYMODESET | _displayMode);
    delayMicroseconds(60);

    /* Initialization ends*/

    command(LCD_DISPLAYCONTROL | _displayControl);
    delayMicroseconds(60);
}

void LiquidCrystal_74HC595_SPI::clear() {
    command(LCD_CLEARDISPLAY);
    delayMicroseconds(3600);
}

void LiquidCrystal_74HC595_SPI::home() {
    command(LCD_RETURNHOME);
    delayMicroseconds(2000);
}

void LiquidCrystal_74HC595_SPI::noDisplay() {
    _displayControl &= ~LCD_DISPLAYON;
    command(LCD_DISPLAYCONTROL | _displayControl);
}

void LiquidCrystal_74HC595_SPI::display() {
    _displayControl |= LCD_DISPLAYON;
    command(LCD_DISPLAYCONTROL | _displayControl);
}

void LiquidCrystal_74HC595_SPI::noCursor() {
    _displayControl &= ~LCD_CURSORON;
    command(LCD_DISPLAYCONTROL | _displayControl);
}

void LiquidCrystal_74HC595_SPI::cursor() {
    _displayControl |= LCD_CURSORON;
    command(LCD_DISPLAYCONTROL | _displayControl);
}

void LiquidCrystal_74HC595_SPI::noBlink() {
    _displayControl &= ~LCD_BLINKON;
    command(LCD_DISPLAYCONTROL | _displayControl);
}

void LiquidCrystal_74HC595_SPI::blink() {
    _displayControl |= LCD_BLINKON;
    command(LCD_DISPLAYCONTROL | _displayControl);
}

void LiquidCrystal_74HC595_SPI::scrollDisplayLeft() {
    command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT);
}

void LiquidCrystal_74HC595_SPI::scrollDisplayRight() {
    command(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT);
}

void LiquidCrystal_74HC595_SPI::leftToRight() {
    _displayMode |= LCD_ENTRYLEFT;
    command(LCD_ENTRYMODESET | _displayMode);
}

void LiquidCrystal_74HC595_SPI::rightToLeft() {
    _displayMode &= ~LCD_ENTRYLEFT;
    command(LCD_ENTRYMODESET | _displayMode);
}

void LiquidCrystal_74HC595_SPI::autoscroll() {
    _displayMode |= LCD_ENTRYSHIFTINCREMENT;
    command(LCD_ENTRYMODESET | _displayMode);
}

void LiquidCrystal_74HC595_SPI::noAutoscroll() {
    _displayMode &= ~LCD_ENTRYSHIFTINCREMENT;
    command(LCD_ENTRYMODESET | _displayMode);
}

void LiquidCrystal_74HC595_SPI::createChar(uint8_t location, uint8_t charmap[]) {
    location &= 0x7;
    command(LCD_SETCGRAMADDR | (location << 3));
    for (uint8_t i = 0; i < 8; i++) {
        write(charmap[i]);
    }
}

void LiquidCrystal_74HC595_SPI::setCursor(uint8_t col, uint8_t row) {
    uint8_t row_offsets[] = {0x00, 0x40, uint8_t(0x00 + _cols), uint8_t(0x40 + _cols)};
    if (row > _rows) {
        row = _rows - 1;
    }
    command(LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

inline void LiquidCrystal_74HC595_SPI::command(uint8_t value) {
    send(value, 0);
}

inline size_t LiquidCrystal_74HC595_SPI::write(uint8_t value) {
    send(value, 1);
    return 1;
}

void LiquidCrystal_74HC595_SPI::send(uint8_t value, uint8_t mode) {
    digitalWrite(_rs, mode);
    transfer(value);
}

void LiquidCrystal_74HC595_SPI::transfer(uint8_t value) {
    digitalWrite(_e, HIGH);
    SPI.beginTransaction(_spiSettings);
    SPI.transfer(value);
    SPI.endTransaction();
    digitalWrite(SS, HIGH);
    digitalWrite(SS, LOW);
    delayMicroseconds(60);
    digitalWrite(_e, LOW);
}
