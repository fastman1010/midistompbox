#include <Arduino.h>
#include <U8g2lib.h>
#include "drawingFunctions.hpp"
#include "types.hpp"

const PROGMEM char labelEnterName[] = "Enter song title:";
char stringBuffer[32];
uint8_t i;

void drawMain(U8G2* u8g2, songEntry* song, bool midiPlaying) {
  u8g2->clearBuffer();
  u8g2->setFont(FONT_BIG);
  u8g2->setDrawColor(COLOR_NORMAL);
  u8g2->drawStr(0, 15, song->title);
  u8g2->updateDisplayArea(0, 0, 16, 2);

  redrawTempo(u8g2, COLOR_NORMAL, song->tempo);
  redrawSlots(u8g2, song->patterns);
  redrawPlayStop(u8g2, midiPlaying);
}

void drawSavedSong(U8G2* u8g2, songEntry* song, int8_t selectedIndex) {
  u8g2->clearBuffer();
  u8g2->setFont(FONT_BIG);
  u8g2->setDrawColor(COLOR_NORMAL);

  sprintf(stringBuffer, "%02d:%s", selectedIndex, song->title);
  u8g2->drawStr(0, 15, stringBuffer);
  u8g2->updateDisplayArea(0, 0, 16, 2);

  redrawSlots(u8g2, song->patterns);
}

void drawMenuActions(U8G2* u8g2, PGM_P const* actionLabels, int8_t selectedIndex) {
  u8g2->clearBuffer();
  u8g2->setFont(FONT_SMALL);

  for (i = 0; i < MENU_SIZE; i++) {
    u8g2->setDrawColor(i != selectedIndex);
    PGM_P pgmAddress = (PGM_P) pgm_read_ptr_near(&(actionLabels[i]));
    strcpy_P(stringBuffer, pgmAddress);
    u8g2->drawStr(0, (i + 1) << 3, stringBuffer);
  }
  u8g2->updateDisplay();
}

void drawTitleInput(U8G2* u8g2, char* title, int8_t selectedIndex) {
  u8g2->clearBuffer();
  u8g2->setFont(FONT_SMALL);
  u8g2->setDrawColor(COLOR_NORMAL);
  strcpy_P(stringBuffer, labelEnterName);
  u8g2->drawStr(0, 8, stringBuffer);

  u8g2->setFont(FONT_BIG);
  for (i = 0; i < strlen(title); i++) {
    u8g2->setDrawColor(i != selectedIndex);
    u8g2->drawGlyph(i * 12, 25, title[i]);
  }
  
  u8g2->updateDisplay();
}

void redrawTempo(U8G2* u8g2, int8_t color, uint8_t tempo) {
  sprintf(stringBuffer, "%3d", tempo);
  u8g2->setFont(FONT_BIG);
  u8g2->setDrawColor(color);
  u8g2->drawStr(92, 15, stringBuffer);
  u8g2->updateDisplayArea(11, 0, 5, 2);
}

void redrawSlots(U8G2* u8g2, uint8_t* patterns, int8_t selectedIndex, int8_t playingIndex) {
  u8g2->setFont(FONT_SMALL);
  for (i = 0; i < NUM_SLOTS; i++) {
    u8g2->setDrawColor(i != selectedIndex);
  /** x, y coordinates
    slot 0: 0,23    slot 1: 32,23   slot 2: 64,23
    slot 3: 0,31    slot 4: 32,31   slot 5: 64,31
    */
   u8g2->drawStr(((i % 3) << 5), (i < 3 ? 23 : 31), getSlotLabel(patterns[i], i == playingIndex));
  }
  u8g2->updateDisplayArea(0, 2, 12, 2);
}

void redrawPlayStop(U8G2* u8g2, bool play) {
  if (play) {
    u8g2->setDrawColor(0);
    u8g2->drawBox(100, 18, 16, 10);
    u8g2->setDrawColor(1);
    u8g2->drawTriangle(100, 18, 116, 23, 100, 27);
  } else {
    u8g2->setDrawColor(1);
    u8g2->drawBox(100, 18, 16, 10);
  }
  u8g2->updateDisplayArea(12, 2, 4, 2);
}

char* getSlotLabel(uint8_t index, bool isPlaying) {
  sprintf(stringBuffer, "%c%02d%c", ('A' + (index >> 4)), (index & 0x0F) + 1, isPlaying ? '<' : ' ');
  return stringBuffer;
}