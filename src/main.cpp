#define USE_TIMER_1     true
#define PIN_ENC_DT      3
#define PIN_ENC_CLK     4
#define BTN_SLOT0       10
#define BTN_SLOT1       11
#define BTN_SLOT2       12
#define BTN_SLOT3       A3
#define BTN_SLOT4       A2
#define BTN_SLOT5       A1
#define BTN_EDIT       2
#define BTN_PLAY       A0

#include <Arduino.h>
#include <MIDI.h>
#include <Encoder.h>
#include <TimerInterrupt.h>
#include <U8g2lib.h>

#include "drawingFunctions.hpp"
#include "types.hpp"

const uint8_t PROGMEM inputPins[] = {BTN_SLOT0, BTN_SLOT1, BTN_SLOT2,
        BTN_SLOT3, BTN_SLOT4, BTN_SLOT5, BTN_EDIT, BTN_PLAY};
const char PROGMEM _menu_label_0[] = "Load";
const char PROGMEM _menu_label_1[] = "Save";
const char PROGMEM _menu_label_2[] = "Back";
PGM_P const PROGMEM songActions[MENU_SIZE]  = {_menu_label_0, _menu_label_1, _menu_label_2};
uint8_t pin;
uint16_t buttonState;
uint8_t encoderTemp;
float tickFrequency;
uint8_t patternNr;
uint8_t patternIndex = 0;
uint8_t songNr = 0;
uint8_t actionNr = 0;
uint8_t inputIndex = 0;
bool midiPlaying = false;
char inputTitle[TITLE_LENGTH + 1] = {'\0'};
songEntry currentSong = {"NONAME", {0, 17, 34, 51, 68, 85}, 120};
songEntry tempSong;
viewsEnum currentView = VIEW_MAIN;

bool inline getState(uint8_t index);
void inline recalculateTickLength();
void inline readAndDisplayTempSongData();
void inline forceTempoOverflow();
void inline forcePatternOverflow();
void inline forceSongOverflow();
void inline forceMenuOverflow();
void inline forceInputCharOverflow();

MIDI_CREATE_DEFAULT_INSTANCE();
Encoder encoder(PIN_ENC_DT, PIN_ENC_CLK);
// for display use default I2C pins: SDA=A4, SCK=A5
U8G2_SH1106_128X32_VISIONOX_F_HW_I2C u8g2(U8G2_R0); 


void sendMIDIClock() {
  MIDI.sendClock();
}

void setup() {
  buttonState = 0x0000;
  for (uint8_t stateIndex = 0; stateIndex < sizeof(inputPins); stateIndex++) {
    pinMode(pgm_read_byte_near(inputPins + stateIndex), INPUT_PULLUP);
  }

  pinMode(13, OUTPUT);
  PORTB |= (1 << 5);

  ITimer1.init();
  MIDI.begin();
  u8g2.begin();
  encoder.write(currentSong.tempo);

  drawMain(&u8g2, &currentSong, midiPlaying);

  recalculateTickLength();
}

void loop() {
  // Get pressed buttons
  for (uint8_t buttonIndex = 0; buttonIndex < sizeof(inputPins); buttonIndex++) {
    pin = pgm_read_byte_near(inputPins + buttonIndex);
    
    if (digitalRead(pin) == HIGH) {
      bitClear(buttonState, buttonIndex);
    } else if (digitalRead(pin) == LOW) {
      if (!getState(buttonIndex)) {
        switch (pin) {
          case BTN_EDIT:
            switch (currentView) {
              case VIEW_MAIN: // enter editing mode from main view
                if (!midiPlaying) { // lock
                  redrawTempo(&u8g2, COLOR_INVERTED, currentSong.tempo);
                  encoder.write(currentSong.tempo);
                  currentView = VIEW_EDIT_TEMPO;
                }
                break;

              case VIEW_EDIT_TEMPO:
              case VIEW_EDIT_SLOT: // exit editing mode
                redrawTempo(&u8g2, COLOR_NORMAL, currentSong.tempo);
                redrawSlots(&u8g2, currentSong.patterns);
                currentView = VIEW_MAIN;
                break;

              case VIEW_SONG_SELECT: // go to action menu
                actionNr = 0;
                drawMenuActions(&u8g2, songActions, actionNr);
                encoder.write(actionNr);
                currentView = VIEW_SONG_ACTION;
                break;

              case VIEW_SONG_ACTION: // select action
                switch (actionNr) {
                  case ACTION_LOAD:
                    eeprom_read_block(&currentSong, (void*) (sizeof(songEntry) * songNr), sizeof(songEntry));
                    strcpy(inputTitle, currentSong.title);
                    drawMain(&u8g2, &currentSong, midiPlaying);
                    recalculateTickLength();
                    currentView = VIEW_MAIN;
                    break;

                  case ACTION_SAVE:
                    inputIndex = 0;
                    drawTitleInput(&u8g2, inputTitle, inputIndex);
                    currentView = VIEW_SONG_TITLE_INPUT;
                    break;

                  case ACTION_BACK:
                    drawSavedSong(&u8g2, &tempSong, songNr);
                    currentView = VIEW_SONG_SELECT;
                    break;

                  default:
                    break;
                }
                break;

              case VIEW_SONG_TITLE_INPUT: // title input
                if (inputTitle[inputIndex] == 0 || inputTitle[inputIndex] == MIN_INPUT_CHAR || inputIndex >= TITLE_LENGTH - 1) {
                  inputTitle[inputIndex] = 0;
                  strcpy(currentSong.title, inputTitle);
                  strcpy(inputTitle, "");
                  eeprom_write_block(&currentSong, (void*) (sizeof(songEntry) * songNr), sizeof(songEntry));
                  drawMain(&u8g2, &currentSong, midiPlaying);
                  currentView = VIEW_MAIN;
                } else {
                  inputIndex++;
                  encoder.write(inputTitle[inputIndex]);
                }
                break;
              default:
                break;
            }
            break;

          case BTN_PLAY:
            switch (currentView) {
              case VIEW_MAIN: // normal operation: MIDI start/stop
                midiPlaying ? MIDI.sendStop() : MIDI.sendStart();
                midiPlaying = !midiPlaying;
                redrawPlayStop(&u8g2, midiPlaying);
                break;

              case VIEW_EDIT_TEMPO:
              case VIEW_EDIT_SLOT: // editing mode: go to saved songs
                currentView = VIEW_SONG_SELECT;
                readAndDisplayTempSongData();
                encoder.write(songNr);
                break;

              case VIEW_SONG_TITLE_INPUT: // title input mode
                if (inputIndex > 0) { // backspace
                  inputIndex--;
                  encoder.write(inputTitle[inputIndex]);
                  drawTitleInput(&u8g2, inputTitle, inputIndex);
                } else { // back to main view
                  strcpy(inputTitle, currentSong.title);
                  drawMain(&u8g2, &currentSong, midiPlaying);
                  currentView = VIEW_MAIN;
                }
                break;

              default: // otherwise go back to main view
                currentView = VIEW_MAIN;
                drawMain(&u8g2, &currentSong, midiPlaying);
                break;
            }
            break;

          /** The following part requires that pattern-changing buttons are in
           * the beginning of inputPins. */
          case BTN_SLOT0:
          case BTN_SLOT1:
          case BTN_SLOT2:
          case BTN_SLOT3:
          case BTN_SLOT4:
          case BTN_SLOT5:
            switch (currentView) {
              case VIEW_EDIT_TEMPO: // in editing mode, switch to pressed slot
                redrawTempo(&u8g2, COLOR_NORMAL, currentSong.tempo);
                currentView = VIEW_EDIT_SLOT;
                // move on
              case VIEW_EDIT_SLOT:
                patternIndex = buttonIndex;
                encoder.write(currentSong.patterns[patternIndex]);
                redrawSlots(&u8g2, currentSong.patterns, patternIndex);
                break;

              case VIEW_MAIN: // normal operation - send Program Change
                patternIndex = buttonIndex;
                MIDI.sendProgramChange(currentSong.patterns[buttonIndex], 8);
                redrawSlots(&u8g2, currentSong.patterns, -1, patternIndex);
                break;

              default:
                break;
            }
            break;

          default:
            break;
        }
      }

      bitSet(buttonState, buttonIndex);
    }

    delay(10); // debounce
  }

  // Rotary encoder operations
  switch (currentView) {
    case VIEW_EDIT_TEMPO:
      currentSong.tempo = encoder.read();
      if (encoderTemp != currentSong.tempo) {
        encoderTemp = currentSong.tempo;
        forceTempoOverflow();
        recalculateTickLength();
        redrawTempo(&u8g2, COLOR_INVERTED, currentSong.tempo);
      }
      break;

    case VIEW_EDIT_SLOT: 
      patternNr = encoder.read();
      if (encoderTemp != patternNr) {
        encoderTemp = patternNr;
        forcePatternOverflow();
        currentSong.patterns[patternIndex] = patternNr;
        redrawSlots(&u8g2, currentSong.patterns, patternIndex);
      }
      break;

    case VIEW_SONG_SELECT: 
      songNr = encoder.read();
      if (encoderTemp != songNr) {
        encoderTemp = songNr;
        forceSongOverflow();
        readAndDisplayTempSongData();
      }
      break;

    case VIEW_SONG_ACTION:
      actionNr = encoder.read();
      if (encoderTemp != actionNr) {
        encoderTemp = actionNr;
        forceMenuOverflow();
        drawMenuActions(&u8g2, songActions, actionNr);
      }
      break;

    case VIEW_SONG_TITLE_INPUT:
      inputTitle[inputIndex] = encoder.read();
      if (encoderTemp != inputTitle[inputIndex]) {
        encoderTemp = inputTitle[inputIndex];
        forceInputCharOverflow();
        drawTitleInput(&u8g2, inputTitle, inputIndex);
      }
      break;

    default:
      encoder.write(currentSong.tempo);
  }
}

bool inline getState(uint8_t index) {
  return (buttonState & (1 << index));
}

void inline recalculateTickLength() {
  /** MIDI clock uses 24 pulses per quarter note. Tempo is given in beats
   * per minute. Need to send 24 * (bpm/60) events per second.
   * So the formula for frequency simplifies to (bpm * 0.4). */
  tickFrequency = currentSong.tempo * 0.4;
  ITimer1.setFrequency(tickFrequency, sendMIDIClock);
}

void inline readAndDisplayTempSongData() {
  eeprom_read_block(&tempSong, (void*) (sizeof(songEntry) * songNr), sizeof(songEntry));
  drawSavedSong(&u8g2, &tempSong, songNr);
}

void inline forceTempoOverflow() {
  if (currentSong.tempo < TEMPO_SLOW) {
    currentSong.tempo = TEMPO_FAST;
    encoder.write(currentSong.tempo);
  } else if (currentSong.tempo > TEMPO_FAST) {
    currentSong.tempo = TEMPO_SLOW;
    encoder.write(currentSong.tempo);
  }
}

void inline forcePatternOverflow() {
  if (patternNr >= INT8_MAX) {
    patternNr = MAX_PATTERNNR;
    encoder.write(patternNr);
  } else if (patternNr > MAX_PATTERNNR) {
    patternNr = 0;
    encoder.write(patternNr);
  }
}

void inline forceSongOverflow() {
  if (songNr >= INT8_MAX) {
    songNr = MAX_SONGNR;
    encoder.write(songNr);
  } else if (songNr > MAX_SONGNR) {
    songNr = 0;
    encoder.write(songNr);
  }
}

void inline forceMenuOverflow() {
  if (actionNr >= INT8_MAX) {
    actionNr = MENU_SIZE - 1;
    encoder.write(actionNr);
  } else if (actionNr >= MENU_SIZE) {
    actionNr = 0;
    encoder.write(actionNr);
  }
}

void inline forceInputCharOverflow() {
  if (inputTitle[inputIndex] >= INT8_MAX) {
    inputTitle[inputIndex] = MIN_INPUT_CHAR;
    encoder.write(inputTitle[inputIndex]);
  } else if (inputTitle[inputIndex] < MIN_INPUT_CHAR) {
    inputTitle[inputIndex] = INT8_MAX - 1;
    encoder.write(inputTitle[inputIndex]);
  }
}